#include<iostream>
#include<queue>
#define pii pair<int,int>
#define mp make_pair
using namespace std;
int main()
{
	int k,n;
	cin>>k>>n;
	int arr[n][n];
	priority_queue<int, vector<int>, greater<int> >pq; 
	for(int i=0;i<k;i++){
		for(int j=0;j<n;j++){
			cin>>arr[i][j];
			pq.push(arr[i][j]);
		}
	}
	while(pq.size()){
    	cout<<pq.top()<<" ";
    	pq.pop();
    }
}	