#include<bits/stdc++.h>
 
using namespace std;
int main() {
 
	int n,k;
    cin>>n>>k;
	priority_queue< long long int> pq;
	int count=1;
	while(n--)
    {
        int q=0;
	    cin>>q;
		if(q==1)
        {
			long long int x ,y;
			cin>>x >>y;
			long long int dist=x*x + y*y;
			if(k>0){
			    pq.push(dist);
			    k--;
			}else if(pq.top()>dist){
			    pq.pop();
			   
			    pq.push(dist);
			}
		}
		else if(q==2){
			 cout<<pq.top()<<endl;
		}
	}
 
	return 0;
}