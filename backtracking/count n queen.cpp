#include<bits/stdc++.h>
// The N-Queen Problem
int ct=0;
bool isSafe(int board[][12],int i,int j, int n) // Helper function to check if it is safe to place the queen
{
	// i-> row, j->col, n-> size of the chess board
	// Checking for the col first
	for(int row=0;row<i;row++){
		// Har row ke "Jth" coloumn par jaake check karlia ki safe he ya nahi
		if(board[row][j]==1)
		{
			return false;
		}
	}
	// Checking for the left diagonal
	int x = i;
	int y = j;

	while(x>=0 && y>=0){
		 if(board[x][y]==1){
		 	return false;
		 }
		 x--;
		 y--;
	}
	// Checking for the right diagonal 
	x = i;
	y = j;
	while(x>=0 && y<n){
		if(board[x][y]==1){
			return false;
		}
		x--;
		y++; 
	}
	// If kahi se false nahi ho raha toh position is safe to place
	return true;
}
bool solveNqueen(int board[][12],int i,int n)
{
	// Base Case-> When all the rows are traversed
	if(i==n){
		ct++;
		// Printing the board
		// for(int i=0;i<n;i++){
		// 	for(int j=0;j<n;j++){
		// 		if(board[i][j]==1){
		// 			// cout<<"Q ";
		// 		}
		// 		// else{
		// 		// 	//cout<<"_ ";
		// 		// }
		// 	}
		// 	// cout<<endl;
		// }
		return false;
	}
	// Recursive Case -> we'll solve the first case and let the recursion handle other case
	for(int j=0;j<n;j++) // Iterating over all the coloumns
	{	
		if(isSafe(board,i,j,n)){
			board[i][j]=1; // yes safe he toh place kardo
			bool nextQueenRakhPaye =  solveNqueen(board,i+1,n);
			if(nextQueenRakhPaye){
				return true;
			} 
		}
		// If upar return true nahi ho paaya so, our assumption is wrong!
		// Means, i,j is not the right position and our assumption is wrong
		board[i][j]=0; 
	}
	return false; 
} 
int main()
{
	int n;
	std::cin>>n;
	int board[12][12]={0}; // Board size
	// i is the starting row, from where we have to start and the n is the total number of rows in nxn matrix
	solveNqueen(board,0,n);
	std::cout<<ct;
}