#include<bits/stdc++.h>
#define ui unsigned int
#define N 1e9
using namespace std;
map<ui,ui>v;
ui f(ui n)
{
	if(n==0){
		return 0;
	}
	if(v[n]!=0){
		return v[n];
	}	
	ui op1 = f(n/2);
	ui op2 = f(n/3);
	ui op3 = f(n/4);
	ui ans = max({n,op1+op2+op3});
	return v[n] = ans;
}
int main()
{
	ui n;
	cin>>n;
	cout<<f(n);
}