#include <iostream>
using namespace std;
int bSearch(int *arr, int n, int ff)
{
    int s = 0, e = n-1, ans = -1;
    while(s<=e)
    {
        int mid = (s+e)/2;
        if(arr[mid]==ff){
            ans = mid;
            s = mid + 1; // upper bound, for lower e = mid -1; we will discard the other part of the array.
        }
        else if(arr[mid]<ff){
            s = mid+1;
        }
        else{
            e = mid-1;
        }
    }
    return ans;
}
int main() {
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    int m;
    cin>>m;
    int ans = bSearch(arr,n,m);
    (ans==-1)?(cout<<-1):(cout<<ans);
}