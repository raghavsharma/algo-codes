#include<iostream>
using namespace std;
string table[] = {"abc","def","ghi","jkl","mno","pqrs","tuv","wx","yz"};
int generateAll(char *inp, char *out, int i, int j,int ctr)
{
	// Base Case
	if(inp[i]=='\0'){
		out[j]='\0';
		cout<<out<<" ";
		return ctr+1;
	}
	// Recursive Case. Smalling for the smaller problem
	int digit = inp[i]-'0'-1;
	for(int k=0;k<table[digit].length();k++){
		out[j] = table[digit][k];
		ctr = generateAll(inp,out,i+1,j+1,ctr);
	}
	return ctr;
}
int main()
{
	char inp[1001];
	cin>>inp;
	char out[1001];
	int ctr = generateAll(inp,out,0,0,0);
	cout<<endl<<ctr;
}