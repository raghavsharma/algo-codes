#include<bits/stdc++.h>
using namespace std;
void shiftLeft(char* inp, int i)
{
	// shift one character to the left.
	for(int j=i;inp[j]!='\0';j++){
		inp[j] = inp[j+1];
	}
}
int main()
{
	char inp[1001];
	cin>>inp;
	int n = strlen(inp);
	for(int j=0;j<n;j++)
	{
		for(int i=0;inp[i]!='\0';i++)
		{
			if(inp[i+1]!='\0' && (inp[i]==inp[i+1]))
			{
				shiftLeft(inp,i);
			}
		}
	}
	cout<<inp;
}

// Using STL string.erase.
// Using Set stl,