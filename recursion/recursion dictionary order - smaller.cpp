#include <bits/stdc++.h>

using namespace std;
bool myCompare(string a, string b)
{
    if(a<b)
    {
        return true;
    }
    return false;
}
void permuate(string input,string str, int i){
    //base case
    if(input[i] == '\0')
    {
        if(myCompare(input,str))
        {
            cout<<input<<endl;
        }
        return;
    }

    //rec case
    for(int j=i; input[j]!='\0'; j++)
    {
        swap(input[i], input[j]);
        permuate(input, str, i+1);
    }
}

int main() 
{
    string input;
    cin>>input;
    string str = input;
    permuate(input, str, 0);
    
}