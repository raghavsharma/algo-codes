// Subsequence using recursion
#include<bits/stdc++.h>
#define pb push_back
using namespace std;
int ctr = 0;
vector<string>v;
void printsub(char *inp, char *out,int i,int j)
{
    if(inp[i]=='\0'){
        out[j]='\0';
        v.pb(out);
        ctr++;
        return;
    }
    out[j]=inp[i];
    printsub(inp,out,i+1,j+1);
    printsub(inp,out,i+1,j);
}
int main()
{
    char inp[100];
    cin>>inp;
    char out[100];
    printsub(inp,out,0,0);
    reverse(v.begin(),v.end());
    for(auto x: v){
        cout<<x<<" ";
    }
    cout<<endl<<ctr;
}