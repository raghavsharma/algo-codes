#include<bits/stdc++.h>
using namespace std;
void shiftRight(char* inp, int j,int n)
{
    for(int i=n+1;i>j;i--)
    {
        inp[i] = inp[i-1];
    }
}
void f(char *inp, int n, int i)
{
	if(i==n){
		return;
	}
	if(i!=n && inp[i]==inp[i+1]){
		shiftRight(inp,i+1,n);
		inp[i+1] = '*';
		n++;
	}
	f(inp,n,i+1);
}
int main()
{
	char inp[10001];
	cin>>inp;
    int n = strlen(inp);
    f(inp,n,0);
    cout<<inp;
}