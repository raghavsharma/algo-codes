#include <bits/stdc++.h>
using namespace std;
int ans = 0;
int fun(string str, int l)
{
    if(l==0)
    {
        return (str[0]-'0');
    }
    fun(str,l-1);
    ans = ans*10 + (str[l-1]-'0'); 
    return ans;
}
int main() {
    string str;
    cin>>str;
    int ans = fun(str,str.length());
    cout<<ans;
}