#include <iostream>
using namespace std;
void allIndices(int *arr, int n, int m)
{
    if(n==0){
        return;
    }
    allIndices(arr,n-1,m);
    if(arr[n-1]==m){
        cout<<n-1<<" ";
    }
}
int main() {
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    int m;
    cin>>m;
    allIndices(arr,n,m);
}