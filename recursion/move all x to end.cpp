#include <bits/stdc++.h>
using namespace std;
void moveatend(char *str, int i, int l)
{
    // base case
    if(i==l-1) return;
    if(str[i]=='x'){
        swap(str[i],str[i+1]);
    }
    for(int j=i;j<l;j++){
        moveatend(str,j+1,l);
    }
}
int main() {
    char str[10001];
    cin>>str;
    int l = strlen(str);
    moveatend(str,0,l);
    cout<<str;
}