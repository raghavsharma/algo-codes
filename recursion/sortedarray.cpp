#include<bits/stdc++.h>
#define ll long long
using namespace std;
bool isSorted(ll *arr, ll n)
{
	if(n==0){
		return true;
	}
	if(arr[n]<arr[n-1]){
		return false;
	}
	return isSorted(arr,n-1);
}
int main()
{
	ll n;
	cin>>n;
	ll a[n];
	for(ll i=0;i<n;i++){
		cin>>a[i];
	}
	if(isSorted(a,n-1)){
		cout<<"true\n";
	}
	else{
		cout<<"false\n";
	}
}