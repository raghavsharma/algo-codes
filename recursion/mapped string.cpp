#include <iostream>
using namespace std;
void generate(char *inp, char *out,int i, int j)
{
    // Base Case : When we reach the end of string, Simply add null in output string, print it and return.
    if(inp[i]=='\0'){
        out[j]='\0';
        cout<<out<<endl;
        return;
    }
    // Extracting current digit 
    int dig = inp[i]-'0';
    char ch = dig - 1 + 'A';
    out[j]=ch;
    generate(inp,out,i+1,j+1);
    if(inp[i+1]!='\0'){
        int sd = inp[i+1]-'0';
        int no = dig*10 + sd;
        if(no<=26){
            char chh = no-1+'A';
            out[j] = chh;
            generate(inp,out,i+2,j+1);
        }
    }
    return;
}
int main() {
    char inp[1000000];
    cin>>inp;
    char out[1000000];
    generate(inp,out,0,0);
}