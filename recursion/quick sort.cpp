// Divide & Conquer Sorting Algorithm
#include <bits/stdc++.h>
using namespace std;
void shuffuleArray(int *arr, int s, int e)
{
    int j;
    srand(time(NULL));
    for(int i=e;i>0;i--){
        j = rand()%(i+1);
        swap(arr[i],arr[j]);
    }
}
int partition(int *arr, int s, int e)
{
    int pivot = arr[e];
    int pIndex = s;
    for(int i=s;i<e;i++)
    {
        if(arr[i]<=pivot){
            swap(arr[i],arr[pIndex]);
            pIndex++;
        }
    }
    swap(arr[pIndex],arr[e]);
    return pIndex;
}
void quickSort(int *arr, int s, int e)
{
    if(s>=e){
        return;
    }
    int pIndex = partition(arr,s,e);
    quickSort(arr,s,pIndex-1);
    quickSort(arr,pIndex+1,e);
}
int main() {
    int n;
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++){
        cin>>a[i];
    }
    srand(time(NULL));
    int s = 0, e = n-1;
    shuffuleArray(a,0,e);
    quickSort(a,s,e);
    for(int i=0;i<n;i++){
        cout<<a[i]<<" ";
    }
}