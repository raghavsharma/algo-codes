#include <iostream>
using namespace std;
void merge(int *a, int s, int e)
{
    int mid = ((s+e)/2);
    int i = s;
    int j = mid+1;
    int k = s;

    int temp[1000000];

    while(i<=mid && j<=e)
    {
        if(a[i]<a[j]){
            temp[k++] = a[i++];
        }
        else{
            temp[k++] = a[j++];
        }
    }
    while(i<=mid)
    {
        temp[k++] = a[i++];
    }
    while(j<=e)
    {
        temp[k++] = a[j++];
    }
    // Copy all elements to original array.
    for(int i=s;i<=e;i++){
        a[i] = temp[i];
    }
}
void mergeSort(int *arr, int s, int e)
{
    if(s>=e){
        return; // base case
    }
    // 1. Divide the array into two
    int mid = ((s+e)/2);
    // 2. Sort the two divided arrays.
    mergeSort(arr,s,mid);
    mergeSort(arr,mid+1,e);
    // 3. Merge the two parts
    merge(arr,s,e);
}
int main() {
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    mergeSort(arr,0,n-1);
    for(int i=0;i<n;i++){
        cout<<arr[i]<<" ";
    }
}