#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll power(ll x, ll y, ll p) 
{ 
    ll res = 1;   
    x = x % p;   
    while (y > 0) 
    { 
       if (y & 1) 
        res = (res*x) % p; 
        y = y>>1; 
        x = (x*x) % p;   
    } 
    return res; 
}
int main() {
    ll a,b,c;
    cin>>a>>b>>c;
    cout<<power(a,b,c);
}