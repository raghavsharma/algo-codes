#include<bits/stdc++.h>
#define F first
#define S second
using namespace std;
bool comp(pair<string,int> a, pair<string, int> b)
{
	if(a.S!=b.S){
		return a.S > b.S;
	}
	else{
		return a.F<b.F;
	}
}
int main()
{
	int x,n,temp;
	cin>>x>>n;
	vector<pair<string,int>> v(n);
	for(int i=0;i<n;i++){
		string s;
		cin>>s>>temp;
		v[i].first = s;
		v[i].second = temp;
	}
	sort(v.begin(),v.end(),comp);
	for(int i=0;i<n;i++){
		if(v[i].S>=x){
			cout<<v[i].F<<" "<<v[i].S<<endl;
		}
	}
}