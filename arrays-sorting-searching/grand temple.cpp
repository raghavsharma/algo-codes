#include<bits/stdc++.h>
using namespace std;
bool comp(pair<int,int>a,pair<int,int>b){
    return a.first < b.first;
}
int main() {
    int n;
    cin>>n;
    vector<int>f(n);
    vector<int>s(n);
    for(int i=0;i<n;i++){
        int a,b;
        cin>>a>>b;
        f[i] = a;
        s[i] = b;
    }
    sort(f.begin(),f.end());
    sort(s.begin(),s.end());
    int maxX = 0, maxY = 0, ans = 0;
    for(int i=1;i<n;i++){
        maxX = max(maxX,abs(f[i]-f[i-1]));
        maxY = max(maxY,abs(s[i]-s[i-1]));
    }
    cout<<((maxX-1)*(maxY-1));
	return 0;
}