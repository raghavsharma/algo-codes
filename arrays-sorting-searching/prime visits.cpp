#include <iostream>
using namespace std;
int pp[5000009];
int ans[5000009];
void sieve(){
    for(int i=2;i*i<=5000009;i++){
        if(pp[i]){
            for(int j=2*i;j<5000009;j+=i)
                pp[j]=0;
            
        }
    }
}

int main() {
    int a,b,p=0,t;
    cin>>t;
    fill(pp,pp+5000009,1);
    sieve();
    while(t--)
    {
        int k=0;
        cin>>a>>b;
        for(int i=2;i<5000009;i++){
            if(pp[i]){
                if(i>=a && i<=b){
                    p++;
                }
                if(i>b) break;
            }            
        }
        cout<<p<<endl;
        p = 0;
    }
}