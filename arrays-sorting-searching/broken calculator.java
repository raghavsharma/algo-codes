import java.math.BigInteger;
import java.util.*;
public class Main {
 
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
        int num = s.nextInt();
        BigInteger fact = BigInteger.valueOf(1); 
        for(int i=num;i>=1;i--)
        {
            fact = fact.multiply(BigInteger.valueOf(i));
        }
        System.out.println(fact);
	}
 
}