#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int main() {
    int n;
    cin>>n;
    vector<int>a;
    vector<int>b;
    for(int i=0;i<n;i++){
        int ii;
        cin>>ii;
        a.pb(ii);
    }
    int m;
    cin>>m;
    for(int i=0;i<m;i++){
        int ii;
        cin>>ii;
        b.pb(ii);
    }
    reverse(a.begin(),a.end());
    reverse(b.begin(),b.end());
    vector<int>ans;
    int ins = 0, sum = 0, carry = 0;
    for(int i=0;i<m || i<n;i++){
        sum = a[i]+b[i]+carry;
        ins = sum%10;
        ans.pb(ins);
        if(sum>9){
            carry = 1;
        }
        else{
            carry = 0;
        }
    }
    if(carry){
        ans.pb(1);
    }
    reverse(ans.begin(),ans.end());
    for(auto x: ans){
        cout<<x<<", ";
    }
    cout<<"END";
}