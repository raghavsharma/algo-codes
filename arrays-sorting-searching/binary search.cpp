#include<iostream>
#define ll long long
using namespace std;
ll bsearch(ll *arr, ll s, ll e, ll k)
{
	while(s<=e)
	{
		ll mid = (s+e)/2;
		if(arr[mid]==k){
			return mid;
		}
		if(arr[mid]>k){
			e = mid -1;
		}
		else{
			s = mid+1;
		}
	}
	return -1;
}
int main()
{
	ll n;
	cin>>n;
	ll arr[n];
	for(ll i=0;i<n;i++){
		cin>>arr[i];
	}
	ll k;
	cin>>k;
	ll ans = bsearch(arr,0,n-1,k);
	cout<<ans;
}