#include<iostream>
using namespace std;
int main() {
  int t;
  cin>>t;
  while(t--)
  {
    int n;
    cin>>n;
    int arr[n];
    int inc[n];
    int dec[n];
    for(int i=0;i<n;i++)
    {
      cin>>arr[i];
    }
    inc[0]=1;
    dec[n-1]=1;
    for(int i=1;i<n;i++)
    {
      inc[i]=(arr[i]>=arr[i-1])?inc[i-1]+1:1;
    }
    for(int i=n-2;i>=0;i--)
    {
      dec[i]=(arr[i]>arr[i+1])?dec[i+1]+1:1;
    }
    int max1=inc[0]+dec[0]-1;
    for(int i=1;i<n;i++)
    {
      max1=max(max1,inc[i]+dec[i]-1);
    }
    cout<<max1<<endl;
  }
	return 0;
}