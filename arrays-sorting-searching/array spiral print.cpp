#include <iostream>
using namespace std;
int main() {
    int n,m;
    cin>>n>>m;
    int a[n][m];
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            cin>>a[i][j];
        }
    }
    int t = 0, b = n-1,l=0,r=m-1;
    int dir = 0;
    // 0 for t to b00
    // 1 for l to r
    // 2 for r to t
    // 3 for r to l
    while(t<=b && l<=r)
    {
        if(dir==0)
        {
            for(int i=t;i<=b;i++){
                cout<<a[i][l]<<", ";
            }
            l++;
        }
        else if(dir==1)
        {
            for(int i=l;i<=r;i++){
                cout<<a[b][i]<<", ";
            }
            b--;
        }
        else if(dir==2)
        {
            for(int i=b;i>=t;i--)
            {
                cout<<a[i][r]<<", ";
            }
            r--;
        }
        else if(dir==3)
        {
            for(int i=r;i>=l;i--){
                cout<<a[t][i]<<", ";
            }
            t++;
        }
        dir = (dir+1)%4;
    }
    cout<<"END";
}