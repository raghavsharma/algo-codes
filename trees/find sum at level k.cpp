#include <iostream>
#include<queue>
#include<bits/stdc++.h>
#define pb push_back
using namespace std;

class Node {
public :
    int noOfChildren ;
    int data ; 
    Node(int d,int n) {
        data = d ;
        noOfChildren = n ;
        right = left = NULL ; 
    }
    Node *right ;
    Node *left ;
};
Node* buildTreeGeneric(Node*root){
    int d,n ;
    cin >> d >> n;
    root = new Node(d,n);
    if(n==0){
        return root ; 
    }
    else if(n==1){
        root->left = buildTreeGeneric(root->left);
        return root ;
    }
    else {
        root->left = buildTreeGeneric(root->left);
        root->right = buildTreeGeneric(root->right) ; 
        return root ;
    }
}
vector<int>v;
void PrintLevelOrder(Node* root, int k)
{
    int p = 0;
	queue<Node*>qq;
	qq.push(root);
	qq.push(NULL);
	// Looping Until our Queue isn't empty
	while(!qq.empty())
	{
		Node* f = qq.front();
		// If front is NULL, then print a new line and insert a NULL at the end of the queue.
		if(!f){
            ++p;
			qq.pop();
		//	cout<<endl;
			if(!qq.empty()){ // If queue is empty and we keep on pushing NULLs then it'll lead to a infinite loop.
				qq.push(NULL);
			}
		}
		else{
            if(p==k)
			   v.pb(f->data);
			qq.pop(); // Remove the Node from the Queue.
			if(f->left!=NULL){
				qq.push(f->left);
			}
			if(f->right!=NULL){
				qq.push(f->right);
			}
		}
	}
	return;
}

int main() {
    Node *root = NULL;
    root = buildTreeGeneric(root); 
    int k;
    cin>>k;
    PrintLevelOrder(root,k);
    int sum = 0;
    for(auto x: v){
        sum+=x;
    }
    cout<<sum;
    return 0;
}