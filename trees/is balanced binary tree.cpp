#include<iostream>
using namespace std;
class Node
{
public:
	int data;
	Node* left;
	Node* right;
	Node(int d)
	{
		data = d;
		left = NULL;
		right = NULL;
	}
};
class HBPair
{
public:
	int height;
	bool balanced;
};
Node* BuildTree(string s)
{
	if(s=="true")
	{
		int data;
		cin>>data;
		Node* root = new Node(data);
		string lt;
		cin>>lt;
		if(lt=="true"){
			root->left = BuildTree(lt);
		}
		string rt;
		cin>>rt;
		if(rt=="true"){
			root->right = BuildTree(rt);
		}
		return root;
	}
	return NULL;
}

HBPair isBalanced(Node* root)
{
	HBPair P;
	if(root==NULL)
	{	
		P.height = -1;
		P.balanced = true;
		return P;
	}
	HBPair left = isBalanced(root->left);
	HBPair right = isBalanced(root->right);
	P.height = max(left.height,right.height) + 1;
	if(abs(left.height-right.height)<=1 && (left.balanced && right.balanced)){
		P.balanced = true;
	}
	else{
		P.balanced = false;
	}
	return P;
}

int main()
{
	Node* root = BuildTree("true");
	HBPair P = isBalanced(root);
	if(isBalanced(root).balanced){
		cout<<"true\n";
	}
	else{
		cout<<"false\n";
	}
}