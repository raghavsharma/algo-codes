#include<iostream>
using namespace std;
class Node
{
public:
	int data;
	Node* left;
	Node* right;
	Node(int d)
	{
		data = d;
		left = NULL;
		right = NULL;
	}
};
class HBPair
{
public:
	int height;
	bool balanced;
};
Node* BuildTree(string s)
{
	if(s=="true")
	{
		int data;
		cin>>data;
		Node* root = new Node(data);
		string lt;
		cin>>lt;
		if(lt=="true"){
			root->left = BuildTree(lt);
		}
		string rt;
		cin>>rt;
		if(rt=="true"){
			root->right = BuildTree(rt);
		}
		return root;
	}
	return NULL;
}

bool isIdentical(Node* r1, Node* r2)
{
	if(r1==NULL && r2==NULL){
		return true;
	}
    if((r1==NULL && r2!=NULL)){
		return false;
	}
	if((r2==NULL && r1!=NULL)){
		return false;
	}
	bool l1 = isIdentical(r1->left,r2->left);
	bool rt1 = isIdentical(r1->right,r2->right);
    return (l1&rt1);
	
}

int main()
{
	Node* r1 = BuildTree("true");
	Node* r2 = BuildTree("true");
	if(isIdentical(r1,r2)){
		cout<<"true\n";
	}
	else{
		cout<<"false\n";
	}
}