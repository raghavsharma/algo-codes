#include<bits/stdc++.h>
using namespace std;
int maxSubArraySum(int* arr, int n)
{
    int ans = INT_MIN;
    for(int i=0;i<n;i++){
        int csum = 0;
        for(int j=i;j<n;j++){
            csum ^= arr[j];
            ans = max(ans,csum);
        }
        
    }
    return ans;
}
int main()
{
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    cout<<maxSubArraySum(arr,n)<<endl;
}