#include<iostream>
using namespace std;
class Node 
{
 public:
	 int data; 
	 Node* next; 
	 Node(int d)
	 {
	 	data = d;
	 	next = NULL;
	 }
};

void Insert(Node *&head, int data)
{
	if(head==NULL){
		head = new Node(data);
		return;
	}
	Node* temp = head;
	while(temp->next!=NULL)
	{
		temp = temp->next;
	}
	Node* nn = new Node(data);
	temp->next = nn;
	return;
}

void print(Node* head)
{
	while(head!=NULL){
		cout<<head->data<<" ";
		head = head->next;
	}
}

Node* MidOfLL(Node* head)
{
	Node* slow = head;
	Node* fast = head->next;

	while(fast!=NULL && fast->next!=NULL){
		slow = slow->next;
		fast = fast->next->next;
	}
	return slow;
}
Node* ReverseLL(Node* head)
{
	if(head==NULL || head->next==NULL){
		return head;
	}
	Node* headsNext = ReverseLL(head->next);
	Node* temp = head;
	temp->next->next = temp;
	temp->next = NULL;
	return headsNext;
}
bool checkPalindrome1(Node* head) // Handling Odd numbers.
{
	Node* first = head;
	Node* mid = MidOfLL(head);
	Node* second = mid->next;
	mid->next = NULL;
	second = ReverseLL(second);
	while(first->next!=mid || second->next!=NULL)
	{
		if((first->data)!=(second->data)){
			return false;
		}
		first = first->next;
		second = second->next;

	}
	return true;
}
bool checkPalindrome2(Node* head) // Handling Even numbers.
{
	Node* first = head;
	Node* mid = MidOfLL(head);
	Node* second = mid->next;
	mid->next = NULL;
	second = ReverseLL(second);
	while(first->next!=NULL || second->next!=NULL)
	{
		if((first->data)!=(second->data)){
			return false;
		}
		first = first->next;
		second = second->next;

	}
	return true;
}
int length(Node* head)
{

}
int main()
{
	Node* head = NULL;
	int n,data;
	cin>>n;
	for(int i=0;i<n;i++){
		cin>>data;
		Insert(head,data);
	}
	if(n&1){
		if(checkPalindrome1(head)){
		cout<<"True\n";
		}
		else{
			cout<<"False\n";
		}
	}
	else{
		if(checkPalindrome2(head)){
		cout<<"True\n";
		}
		else{
			cout<<"False\n";
		}
	}
	return 0;
}