#include<iostream>
using namespace std;
class Node 
{
 public:
	 int data; 
	 Node* next; 
	 Node(int d)
	 {
	 	data = d;
	 	next = NULL;
	 }
};
Node* MidOfLL(Node* head)
{
	Node* slow = head;
	Node* fast = head->next;
	while(fast!=NULL && fast->next!=NULL)
	{
		slow = slow->next;
		fast = fast->next->next;
	}
	return slow;
}
Node* MergeSortedLLRecursive(Node* a, Node* b)
{
	// Base Case
	if(a==NULL){
		return b;
	}
	if(b==NULL){
		return a;
	}
	Node* temp; // Pointer to store the address of sorted merged a LL.
	if((a->data)<=(b->data)){
		temp = a;
		temp->next = MergeSortedLLRecursive(a->next,b);
	}
	else{
		temp = b;
		temp->next = MergeSortedLLRecursive(a,b->next);
	}
	return temp;
}
void Insert(Node *&head, int data)
{
	if(head==NULL){
		head = new Node(data);
		return;
	}
	Node* temp = head;
	while(temp->next!=NULL)
	{
		temp = temp->next;
	}
	Node* nn = new Node(data);
	temp->next = nn;
	return;
}
void print(Node* head)
{
	while(head!=NULL){
		cout<<head->data<<" ";
		head = head->next;
	}
}
int main()
{
	int tc;
	cin>>tc;
	while(tc--)
	{
		int x,y,data;
		cin>>x;
		Node* a = NULL;
		Node* b = NULL;
		for(int i=0;i<x;i++){
			cin>>data;
			Insert(a,data);
		}
		cin>>y;
		for(int i=0;i<y;i++){
			cin>>data;
			Insert(b,data);
		}
		Node* c = MergeSortedLLRecursive(a,b);
		print(c);
		cout<<endl;
	}
	return 0;
}